
/*--------------------------------------------------------

    Main Scripts

    [Table of contents]
        01. CAROUSEL
        02. RANDOM HEADER
        03. HEADER CONTENT OPACITY
        04. NAVBAR
        05. STICKY NAVBAR
        06. MAGNIFIC POPUP
        07. COUNTERS
        08. NAVBAR WIDTH FIX
        09. CONTACT FORM
        10. SCROLL TO TOP
        11. SMOOTH SCROLL
        12. PAGE ANIMATIONS
        13. PRELOADER
        14. ISOTOPE

--------------------------------------------------------*/

(function(){
    'use strict';
    $(window).ready(function(){

        /*--------------------------------------------------------
            01. CAROUSEL
        --------------------------------------------------------*/

        
        $(function(){
            
            $("#owl-demo").owlCarousel({
               navigation: false,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                transitionStyle: "fade",
                mouseDrag: false,
                touchDrag: false,
                autoPlay: true

              // "singleItem:true" is a shortcut for:
              // items : 1, 
              // itemsDesktop : false,
              // itemsDesktopSmall : false,
              // itemsTablet: false,
              // itemsMobile : false

          });
            
        });

        /*--------------------------------------------------------
            02. RANDOM HEADER
        --------------------------------------------------------*/
        $(function(){
            var random = Math.ceil(Math.random() * 3),
                newHeader = "header-" + 1;
            $('.head').addClass(newHeader);
        });

        /*--------------------------------------------------------
            03. HEADER CONTENT OPACITY
        --------------------------------------------------------*/
        /*$(function(){
           
            var element = $('.caption .item'),
                offsetEnd = 450,
                opacity,
                offset = $(document).scrollTop();
            offset > offsetEnd ? opacity = 0 : opacity = 1 - offset/offsetEnd;
            element.css("opacity", opacity);
            $(window).scroll(function(){
                offset = $(document).scrollTop();
                offset <= offsetEnd ? opacity = 1 - offset/offsetEnd : opacity = 0;
                element.css('opacity', opacity);
            });
        });*/

        /*--------------------------------------------------------
            04. NAVBAR
        --------------------------------------------------------*/
        $(function(){
            var head = $('.head').height(),
                navbar = $('.navbar');
            $(this).scrollTop() >= head ? navbar.removeClass('normal').addClass('offset') : navbar.removeClass('offset').addClass('normal');
            $(window).scroll(function () {
                $(this).scrollTop() >= head ? navbar.removeClass('normal').addClass('offset') : navbar.removeClass('offset').addClass('normal');
            });
        });

        /*--------------------------------------------------------
            05. STICKY NAVBAR
        --------------------------------------------------------*/
        $(function(){
            $('.navbar').sticky({
                topSpacing: 0
            });
        });

        /*--------------------------------------------------------
            06. MAGNIFIC POPUP
        --------------------------------------------------------*/
        $(function(){
            $('#works .grid').magnificPopup({
                delegate: 'a',
                type: 'image'
            });
        });

        /*--------------------------------------------------------
            07. COUNTERS
        --------------------------------------------------------*/
        $(function(){
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        });

        /*--------------------------------------------------------
            08. NAVBAR WIDTH FIX
        --------------------------------------------------------*/
        $(function(){
            var width = $('.head').width();
            $('.navbar').css('max-width', width);
            $(window).resize(function() {
                var width = $('.head').width();
                $('.navbar').css('max-width', width);
            });
        });

        /*--------------------------------------------------------
            09. CONTACT FORM
        --------------------------------------------------------*/
        $("form button").click(function() {
            var user_name = $('input[name=input-name]').val(),
                user_email = $('input[name=input-email]').val(),
                user_subject = $('input[name=input-subject]').val(),
                user_message = $('textarea[name=input-message]').val(),
                process = true;
            if (user_name == "") {
                $('input[name=input-name]').css('border-color','#ed5564');
                process = false;
            }

            if (user_email == "") {
                $('input[name=input-email]').css('border-color','#ed5564');
                process = false;
            }

            if (user_subject == "") {
                $('input[name=input-subject]').css('border-color','#ed5564');
                process = false;
            }

            if (user_message == "") {
                $('textarea[name=input-message]').css('border-color','#ed5564');
                process = false;
            }

            if (process) {
                post_data = {'userName':user_name, 'userEmail':user_email, 'userSubject':user_subject, 'userMessage':user_message};
                $.post('contact_me.php', post_data, function(response){     /* change your email and subject in contact_me.php */
                    if(response.type == 'error') {
                        output = '<div class="error">'+response.text+'</div>';
                    }
                    else {
                        output = '<div class="success">'+response.text+'</div>';
                        $('form input').val('');
                        $('form textarea').val('');
                    }
                    $("#result").hide().html(output).slideDown();
                }, 'json');
            }
        });
        $("form input, form textarea").keyup(function() {
            $("form input, form textarea").css('border-color','');
            $("#result").slideUp();
        });


        /*--------------------------------------------------------
            10. SCOLL TO TOP
        --------------------------------------------------------*/
        $(function(){
            if ($(window).width() > 850) {
                $(window).scroll(function(){
                    $(this).scrollTop() > 500 ? $('.scrolltop').fadeIn() : $('.scrolltop').fadeOut();
                });
                $('.scrolltop').click(function(){
                    $('html, body').animate({scrollTop: 0}, 500);
                    return false;
                });
            }
        });

        /*--------------------------------------------------------
            11. SMOOTH SCROLL
        --------------------------------------------------------*/
        $(function(){
            var height = $('nav').height();
            $('.scroll').click(function(){
                var href = $(this).attr("href");
                var hash = href.substr(href.indexOf("#"));
                if (hash == '#home') {
                    $('html, body').animate({scrollTop: 0}, 500);
                }
                else {
                    $('html,body').animate({scrollTop:$(this.hash).offset().top - height}, 500);
                }
            });
        });

        /*--------------------------------------------------------
            12. PAGE ANIMATIONS
        --------------------------------------------------------*/
        $(function(){
            new WOW().init();
        });
    }); /* end of ready function */


    $(window).load(function(){

        /*--------------------------------------------------------
            13. PRELOADER
        --------------------------------------------------------*/
        $(function(){
            $('#status').fadeOut();
            $('#preloader').delay(350).fadeOut('slow');
            $('body').delay(350).css({'overflow':'visible'});
        });

        /*--------------------------------------------------------
            14. ISOTOPE
        --------------------------------------------------------*/
        $(function(){
            var $container = $('#works .grid');
            $container.imagesLoaded(function() {
                $container.isotope({
                    itemSelector: '.item',
                    layoutMode: 'fitRows'
                });
            });
            $('#works .filters li').click(function() {
                $('#works .filters li').removeClass('active');
                $(this).addClass('active');
                var filter = $(this).attr('data-filter');
                $('#works .item').each(function(){
                    $(this).find('a').css({
                        'cursor': 'pointer'
                    });
                    $(this).css({
                        '-webkit-transform': 'scale(1)',
                        '-moz-transform': 'scale(1)',
                        '-o-transform': 'scale(1)',
                        'transform': 'scale(1)',
                        'opacity': 1
                    });
                    if (filter != '*' && !$(this).hasClass(filter)) {
                        $(this).find('a').css({
                            'cursor': 'not-allowed'
                        });
                        $(this).css({
                            '-webkit-transform': 'scale(0.8)',
                            '-moz-transform': 'scale(0.8)',
                            '-o-transform': 'scale(0.8)',
                            'transform': 'scale(0.8)',
                            'opacity': '0.3'
                        });
                    }
                });
            });
        });
    }); /* end of load function */
})(jQuery);