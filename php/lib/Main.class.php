<?php

class Main {
	public function __construct(){}
	static function main() {
		$connection = sys_db_Sqlite::open("site.db");
		sys_db_Manager::set_cnx($connection);
		sys_db_Manager::initialize();
		$slideshow = Slideshow::$manager->all(null);
		$portfolio = Portfolio::$manager->all(null);
		$categories = PortfolioCategory::$manager->all(null);
		$team = Team::$manager->all(null);
		$contacts = Contacts::$manager->unsafeGet(1, true);
		$services = Service::$manager->all(null);
		$sections = Section::$manager->all(null);
		$siteInfo = SiteInfo::$manager->unsafeGet(1, true);
		$siteInfo->year = Date::now()->getFullYear();
		$siteInfo->services = Section::$manager->unsafeGet(1, true);
		$siteInfo->works = Section::$manager->unsafeGet(2, true);
		$siteInfo->team = Section::$manager->unsafeGet(3, true);
		$siteInfo->contacts = Section::$manager->unsafeGet(4, true);
		$templateCode = sys_io_File::getContent("theme/index.html");
		$template = new haxe_Template($templateCode);
		$output = $template->execute(_hx_anonymous(array("siteInfo" => $siteInfo, "contacts" => $contacts, "services" => $services, "team" => $team, "portfolio" => $portfolio, "categories" => $categories, "slideshow" => $slideshow)), null);
		php_Lib::hprint($output);
		sys_db_Manager::cleanup();
		$connection->close();
	}
	function __toString() { return 'Main'; }
}
