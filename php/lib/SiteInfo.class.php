<?php

class SiteInfo extends sys_db_Object {
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		parent::__construct();
	}}
	public $id;
	public $title;
	public $logo;
	public $logoInvert;
	public $favicon;
	public $facebook;
	public $instagram;
	public $google;
	public $yahoo;
	public $twitter;
	public $year;
	public $services;
	public $works;
	public $team;
	public $contacts;
	public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->__dynamics[$m]) && is_callable($this->__dynamics[$m]))
			return call_user_func_array($this->__dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call <'.$m.'>');
	}
	static function __meta__() { $args = func_get_args(); return call_user_func_array(self::$__meta__, $args); }
	static $__meta__;
	static $manager;
	function __toString() { return 'SiteInfo'; }
}
SiteInfo::$__meta__ = _hx_anonymous(array("obj" => _hx_anonymous(array("rtti" => (new _hx_array(array("oy4:namey8:SiteInfoy7:indexesahy9:relationsahy7:hfieldsby4:logooR0R5y6:isNullfy1:tjy17:sys.db.RecordType:15:0gy2:idoR0R9R6fR7jR8:1:0gy10:logoInvertoR0R10R6fR7r5gy7:twitteroR0R11R6fR7r5gy7:faviconoR0R12R6fR7r5gy5:yahoooR0R13R6fR7r5gy6:googleoR0R14R6fR7r5gy8:facebookoR0R15R6fR7r5gy9:instagramoR0R16R6fR7r5gy5:titleoR0R17R6fR7r5ghy3:keyaR9hy6:fieldsar6r15r4r8r10r13r14r12r11r9hg")))))));
SiteInfo::$manager = new sys_db_Manager(_hx_qtype("SiteInfo"));
