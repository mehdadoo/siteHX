<?php

class Team extends sys_db_Object {
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		parent::__construct();
	}}
	public $id;
	public $name;
	public $role;
	public $facebook;
	public $email;
	public $website;
	public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->__dynamics[$m]) && is_callable($this->__dynamics[$m]))
			return call_user_func_array($this->__dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call <'.$m.'>');
	}
	static function __meta__() { $args = func_get_args(); return call_user_func_array(self::$__meta__, $args); }
	static $__meta__;
	static $manager;
	function __toString() { return 'Team'; }
}
Team::$__meta__ = _hx_anonymous(array("obj" => _hx_anonymous(array("rtti" => (new _hx_array(array("oy4:namey4:Teamy7:indexesahy9:relationsahy7:hfieldsby2:idoR0R5y6:isNullfy1:tjy17:sys.db.RecordType:1:0gR0oR0R0R6fR7jR8:15:0gy4:roleoR0R9R6fR7r7gy5:emailoR0R10R6fR7r7gy7:websiteoR0R11R6fR7r7gy8:facebookoR0R12R6fR7r7ghy3:keyaR5hy6:fieldsar4r6r8r11r9r10hg")))))));
Team::$manager = new sys_db_Manager(_hx_qtype("Team"));
