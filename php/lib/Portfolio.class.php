<?php

class Portfolio extends sys_db_Object {
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		parent::__construct();
	}}
	public $id;
	public $image;
	public $link;
	public $title;
	public $description;
	public $category;
	public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->__dynamics[$m]) && is_callable($this->__dynamics[$m]))
			return call_user_func_array($this->__dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call <'.$m.'>');
	}
	static function __meta__() { $args = func_get_args(); return call_user_func_array(self::$__meta__, $args); }
	static $__meta__;
	static $manager;
	function __toString() { return 'Portfolio'; }
}
Portfolio::$__meta__ = _hx_anonymous(array("obj" => _hx_anonymous(array("rtti" => (new _hx_array(array("oy4:namey9:Portfolioy7:indexesahy9:relationsahy7:hfieldsby2:idoR0R5y6:isNullfy1:tjy17:sys.db.RecordType:1:0gy5:imageoR0R9R6fR7jR8:15:0gy5:titleoR0R10R6fR7r7gy11:descriptionoR0R11R6fR7r7gy4:linkoR0R12R6fR7r7gy8:categoryoR0R13R6fR7r7ghy3:keyaR5hy6:fieldsar4r6r10r8r9r11hg")))))));
Portfolio::$manager = new sys_db_Manager(_hx_qtype("Portfolio"));
