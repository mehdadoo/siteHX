class Team extends sys.db.Object
{
    public var id : Int;
    public var name : String;
    public var role : String;
    public var facebook : String;
    public var email : String;
    public var website : String;
    
    public static var manager = new sys.db.Manager<Team>(Team);
}