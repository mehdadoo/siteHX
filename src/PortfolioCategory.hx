@:table("PortfolioCategories")
class PortfolioCategory extends sys.db.Object
{
    public var id : Int;
    public var name : String;
    
    public static var manager = new sys.db.Manager<PortfolioCategory>(PortfolioCategory);
}