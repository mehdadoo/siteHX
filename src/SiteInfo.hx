class SiteInfo extends sys.db.Object
{
    public var id           : Int;
    public var title        : String;
    public var logo         : String;
    public var logoInvert   : String;
    public var favicon      : String;
    
    public var facebook : String;
    public var instagram: String;
    public var google   : String;
    public var yahoo    : String;
    public var twitter  : String;
    
    @:skip
    public var year     :Int;
    @:skip
    public var services :Section;
    @:skip
    public var works    :Section;
    @:skip
    public var team     :Section;
    @:skip
    public var contacts :Section;
    
    public static var manager = new sys.db.Manager<SiteInfo>(SiteInfo);
}