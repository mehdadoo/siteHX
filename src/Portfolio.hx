class Portfolio extends sys.db.Object
{
    public var id : Int;
    public var image : String;
    public var link : String;
    public var title : String;
    public var description : String;
    public var category : String;
    
    public static var manager = new sys.db.Manager<Portfolio>(Portfolio);
}