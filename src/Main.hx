import sys.db.*;

class Main 
{
    public static function main()
    {
        var connection : Connection = Sqlite.open("site.db");
        sys.db.Manager.cnx = connection;
        sys.db.Manager.initialize();
        
		var slideshow:List<Slideshow> = Slideshow.manager.all();
        var portfolio:List<Portfolio> = Portfolio.manager.all();
        var categories:List<PortfolioCategory> = PortfolioCategory.manager.all();
        var team:List<Team>         = Team.manager.all();
        var contacts:Contacts       = Contacts.manager.get(1);
        var services:List<Service>  = Service.manager.all();
		var sections:List<Section>  = Section.manager.all();
        var siteInfo:SiteInfo = SiteInfo.manager.get(1);
        
        siteInfo.year       = Date.now().getFullYear();
        siteInfo.services   = Section.manager.get(1);
        siteInfo.works      = Section.manager.get(2);
        siteInfo.team       = Section.manager.get(3);
        siteInfo.contacts   = Section.manager.get(4);
        
        
        var templateCode:String = sys.io.File.getContent("theme/index.html");

        //Execute it
        var template:haxe.Template = new haxe.Template(templateCode);
        var output:String = template.execute({siteInfo: siteInfo, contacts: contacts, services:services, team:team, portfolio:portfolio, categories:categories, slideshow:slideshow});

        #if neko
            neko.Lib.print( output );
        #else
            php.Lib.print( output );
        #end
            
        
        sys.db.Manager.cleanup();
        connection.close();
    }
}