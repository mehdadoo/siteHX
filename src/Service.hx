@:table("services")
class Service extends sys.db.Object
{
    public var id : Int;
    public var icon : String;
    public var title : String;
    public var description : String;
    
    public static var manager = new sys.db.Manager<Service>(Service);
}