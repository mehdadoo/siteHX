class Slideshow extends sys.db.Object
{
    public var id : Int;
    public var title : String;
    public var description : String;
  
    public static var manager = new sys.db.Manager<Slideshow>(Slideshow);
}