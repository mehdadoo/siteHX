@:table("sections")
class Section extends sys.db.Object
{
    public var id : Int;
    public var title : String;
    public var description : String;
	public var type : String;
    
    public static var manager = new sys.db.Manager<Section>(Section);
}