class Contacts extends sys.db.Object
{
    public var id : Int;
    public var address : String;
    public var email : String;
    public var phone : String;
    
    public static var manager = new sys.db.Manager<Contacts>(Contacts);
}